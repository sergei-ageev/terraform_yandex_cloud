resource "yandex_vpc_address" "this" {
  name = "public-ip"
  external_ipv4_address {
    zone_id = yandex_vpc_subnet.this.zone
  }
}