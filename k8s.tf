resource "yandex_kubernetes_cluster" "this" {
 labels = {
    environment  = "test"
  }

 network_id = "${yandex_vpc_network.this.id}"
 master {
   version = var.k8s-settings.version
   public_ip = true
   maintenance_policy {
      auto_upgrade = false
    }
   zonal {
     zone      = "${yandex_vpc_subnet.this.zone}"
     subnet_id = "${yandex_vpc_subnet.this.id}"
   }
 }
 service_account_id      = "${yandex_iam_service_account.this.id}"
 node_service_account_id = "${yandex_iam_service_account.this.id}"
   depends_on = [
     yandex_resourcemanager_folder_iam_member.k8s-clusters-agent,
     yandex_resourcemanager_folder_iam_member.vpc-public-admin,
     yandex_resourcemanager_folder_iam_member.images-puller

   ]

 release_channel = "STABLE"
 network_policy_provider = "CALICO"

 kms_provider {
    key_id = yandex_kms_symmetric_key.this.id
  }
}

resource "yandex_vpc_network" "this" {
  name = "k8s-network"
}

resource "yandex_vpc_subnet" "this" {
 v4_cidr_blocks = [var.network-settings.subnet_ip]
 zone           = "${var.cloud-settings.access_zone}"
 network_id     = "${yandex_vpc_network.this.id}"
}

resource "yandex_iam_service_account" "this" {
 name        = "${var.service-accounts-k8s.sa-k8s}"
 description = "Service accout for k8s"
}

resource "yandex_resourcemanager_folder_iam_member" "k8s-clusters-agent" {
  # Сервисному аккаунту назначается роль "k8s.clusters.agent".
  folder_id = var.cloud-settings.folder_id
  role      = "k8s.clusters.agent"
  member    = "serviceAccount:${yandex_iam_service_account.this.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "vpc-public-admin" {
  # Сервисному аккаунту назначается роль "vpc.publicAdmin".
  folder_id = var.cloud-settings.folder_id
  role      = "vpc.publicAdmin"
  member = "serviceAccount:${yandex_iam_service_account.this.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "images-puller" {
  # Сервисному аккаунту назначается роль "container-registry.images.puller".
  folder_id = var.cloud-settings.folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.this.id}"
}

resource "yandex_kms_symmetric_key" "this" {
  # Ключ для шифрования важной информации, такой как пароли, OAuth-токены и SSH-ключи.
  name              = "kms-key-k8s"
  default_algorithm = "AES_128"
  rotation_period   = "8760h" # 1 год.
}

resource "yandex_kms_symmetric_key_iam_binding" "viewer" {
  symmetric_key_id = yandex_kms_symmetric_key.this.id
  role             = "viewer"
  members = [
    "serviceAccount:${yandex_iam_service_account.this.id}",
  ]
}