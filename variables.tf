variable "cloud-settings" {
  type = object({
    cloud_id = string
    folder_id = string
    access_zone = string
  })
}

variable "k8s-settings" {
  type = object({
    version = string
    container_runtime = string
    node_platform_id = string
  })
}

variable "network-settings" {
  type = object({
  subnet_name = string
  subnet_ip = string
  })
}

variable "service-accounts-k8s" {
  type = object ({
    # sa-name-k8s-res = string
    # sa-name-k8s-node = string
    sa-k8s = string
  })
}

variable "postgresql-settings" {
  type = object({
  cluster_name = string
  environment = string
  deletion_protection = bool
  version = number

  resource_preset_id = string
  disk_type_id       = string
  disk_size          = number
  })
}

variable "postgresql_db" {
  type = string
  description = "Name of the DB"
}

variable "postgresql_user" {
  type = string
  description = "Database user"
}

variable "postgresql_password" {
  type = string
  description = "Database password"
}