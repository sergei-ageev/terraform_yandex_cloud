resource "yandex_kubernetes_node_group" "this" {
  cluster_id = "${yandex_kubernetes_cluster.this.id}"
  name       = "node-group-1"
  instance_template {
    # https://registry.tfpla.net/providers/yandex-cloud/yandex/latest/docs/resources/kubernetes_node_group
    name       = "" # use default name {instance_group.id}-{instance.short_id}
    platform_id = var.k8s-settings.node_platform_id
    container_runtime {
     type = var.k8s-settings.container_runtime
    }
    network_interface {
      nat = true # выход в интернет для скачивания образов
      subnet_ids = ["${yandex_vpc_subnet.this.id}"]
    }
  }
  scale_policy {
    fixed_scale {
      size = 1 # количество узлов в группе
    }
  }
  allocation_policy {
    location {
      zone = var.cloud-settings.access_zone
    }
  }
}