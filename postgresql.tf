resource "yandex_mdb_postgresql_cluster" "this" {
  name                = var.postgresql-settings.cluster_name
  environment         = var.postgresql-settings.environment
  network_id          = yandex_vpc_network.this.id
  security_group_ids  = [ yandex_vpc_security_group.pgsql-sg.id ]
  deletion_protection = var.postgresql-settings.deletion_protection

  config {
    version = var.postgresql-settings.version
    access {
    web_sql = true
    }
    resources {
      resource_preset_id = var.postgresql-settings.resource_preset_id
      disk_type_id       = var.postgresql-settings.disk_type_id
      disk_size          = var.postgresql-settings.disk_size
    }
  }

  host {
    zone      = yandex_vpc_subnet.this.zone
    name      = "this"
    subnet_id = yandex_vpc_subnet.this.id
  }
}

resource "yandex_mdb_postgresql_database" "this" {
  cluster_id = yandex_mdb_postgresql_cluster.this.id
  name       = var.postgresql_db
  owner      = var.postgresql_user
}

resource "yandex_mdb_postgresql_user" "this" {
  cluster_id = yandex_mdb_postgresql_cluster.this.id
  name       = var.postgresql_user
  password   = var.postgresql_password
}

resource "yandex_vpc_security_group" "pgsql-sg" {
  name       = "pgsql-sg"
  network_id = yandex_vpc_network.this.id

  ingress {
    description    = "PostgreSQL"
    port           = 6432
    protocol       = "TCP"
    v4_cidr_blocks = [ "10.0.0.0/8" ]
  }
}
