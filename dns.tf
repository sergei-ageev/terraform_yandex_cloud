#  sdevops.ru

resource "yandex_dns_zone" "sdevops" {
  zone = "sdevops.ru."

  name        = "sdevops-ru"
  description = "My public zone for sdevops.ru"

  labels = {
    app = "yelb"
  }

  public           = true
}

resource "yandex_dns_recordset" "sdevops" {
  zone_id = yandex_dns_zone.sdevops.id
  name    = "sdevops.ru."
  type    = "A"
  ttl     = 600
  data    = ["158.160.56.69"] # Белый IP адрес K8S Worker Node
}