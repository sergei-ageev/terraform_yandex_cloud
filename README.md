# Описание
Файлы для развертывания инфраструктуры в Yandex Cloud для проекта:
https://gitlab.com/sergei-ageev/cicd_helm_kubernetes

Управление инфраструктурой производится с использованием CI/CD. Состояние хранится в объектом хранилище в Yandex Cloud.
