terraform {
  required_version = ">=1.0.0"

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "> 0.8"
    }
  }
  backend "s3" {
    endpoint = "storage.yandexcloud.net"
    region   = "ru-central1"

    bucket = "tf-state-ageev"

    skip_region_validation      = true
    skip_credentials_validation = true

    dynamodb_endpoint = "https://docapi.serverless.yandexcloud.net/ru-central1/b1g5gigmhi4s8sacg22h/etn7gooa6f3u372h7d2c"
    dynamodb_table    = "table-lockid"

    key = "terraform.tfstate"
  }
}

provider "yandex" {
  cloud_id = "b1g5gigmhi4s8sacg22h"
  folder_id = "b1g3uq7m3g08fflheig5"
}
