k8s-settings = ({
  version = "1.23"
  container_runtime = "docker" # or containerd
  node_platform_id = "standard-v2" # Intel Cascade Lake
})

cloud-settings = ({
  cloud_id = "b1g5gigmhi4s8sacg22h"
  folder_id = "b1g3uq7m3g08fflheig5"
  access_zone = "ru-central1-a"
})

network-settings = ({
  subnet_name = "default-ru-central1-d"
  subnet_ip = "10.131.0.0/24"
})

service-accounts-k8s = ({
  sa-k8s = "sa-k8s"
})

postgresql-settings = ({
  cluster_name = "pg-cluster"
  environment = "PRODUCTION"
  deletion_protection = false
  version = 14

  resource_preset_id = "s2.micro"
  disk_type_id       = "network-ssd"
  disk_size          = 10
})